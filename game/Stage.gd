extends Node

var Block = preload("res://entities/Block.tscn")
var Shard = preload("res://entities/Shard.tscn")

var blocks := []
var shards := []

var rng := RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
#	randomize()
#	seed("ld49".hash())
	rng.randomize()
	
	$BlockSpawner.wait_time = 6
	$ShardSpawner.wait_time = 15
	$BlockSpawner.start()
	$ShardSpawner.start()
	
#	spawn_block(100)
	spawn_block(200)
	spawn_block(350)
	spawn_block(500)
	spawn_block(650)

	spawn_shard(550)

func spawn_block(pos_x: int = -1) -> void:
	var block = Block.instance()
	blocks.append(block)
#	block.visible = false
#	block.position.x = rng.randi_range($Camera2D.position.x + 320 + 64, $Camera2D.position.x + 320 + 128)
	if pos_x >= 0:
		block.position.x = pos_x
	else:
		block.position.x = $Camera2D.position.x + 640 + rng.randi_range(32, 64)
	block.position.y = rng.randi_range(90, 360)
	block.rotation_degrees = rng.randi_range(-20, 20)
#	print block.rotation
	
	add_child(block)
	
#	yield(get_tree().create_timer(1.0), "timeout")
#	block.mode = RigidBody2D.MODE_STATIC


func spawn_shard(pos_x: int = -1) -> void:
	var shard = Shard.instance()
	shards.append(shard)
	if pos_x >= 0:
		shard.position.x = pos_x
	else:
		shard.position.x = $Camera2D.position.x + 640 + rng.randi_range(32, 64)
	shard.position.y = rng.randi_range(10, 350)
	add_child(shard)


func _on_BlockSpawner_timeout() -> void:
	spawn_block()

func _on_ShardSpawner_timeout() -> void:
	spawn_shard()
