extends RigidBody2D

var velocity = Vector2()

var fuel: float = 100.0
var burn_rate: float = 5
var refuel_rate: float = 5
var on_platform: bool = false
#var docked: bool = false
var boost_ready: bool = false
var boost_bonus: float = 0
var time_of_landing: float = 0
var time_of_takeoff: float = 0
var time_on_ground: float = 0
var time_in_air: float = 0
var stabilized: bool = false
var stabilizing: bool = false

var shards: int = 0

var input_enabled: bool = false
#var 

#func get_input():
#	velocity = Vector2()
#	if Input.is_action_pressed("right"):
#		velocity.x += 1
#	if Input.is_action_pressed("left"):
#		velocity.x -= 1
#	if Input.is_action_pressed("down"):
#		velocity.y += 1
#	if Input.is_action_pressed("up"):
#		velocity.y -= 1
#	velocity = velocity.normalized() * speed

func _ready():
	input_enabled = true
	$Light2D.energy = 0

func _physics_process(delta):
	if on_platform:
		fuel += 1 * refuel_rate * delta
		time_on_ground = OS.get_ticks_msec() - time_of_landing
		if time_on_ground > 1000 and ! boost_ready:
			$AnimationPlayer.play("glow")
			$Powerup.play()
			boost_ready = true
	else:
		time_in_air = OS.get_ticks_msec() - time_of_takeoff
		if time_in_air > 100 and ! stabilized:
#			stabilize()
			stabilizing = true
			if boost_ready:
				$AnimationPlayer.play("glow_off")
				boost_ready = false
			

func shard_collected():
	shards += 1
	$Pickup.play()

#func stabilize():
#	stabilized = true
	
#	$Tween.interpolate_property($Sprite, "rotation_degrees",
#			rotation_degrees, 0, 1000,
#			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
#	$Tween.start()

			
func _integrate_forces(state):
	if not input_enabled:
		return
	
	velocity = Vector2()
	if Input.is_action_pressed("right") or Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("left") or Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("down") or Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("up") or Input.is_action_pressed("ui_up"):
		velocity.y -= 1

	if velocity.length() > 0:
		if fuel > 0:
			fuel -= 1 * burn_rate * state.step
			if fuel < 0: fuel = 0
			if not $Thrusters.playing: $Thrusters.play()

			if boost_bonus > 0:
				velocity *= boost_bonus
				boost_bonus -= 10
		else:
			velocity = Vector2()
	else:
		$Thrusters.stop()		

	applied_force = velocity * 1800 * state.step


	if stabilizing:
		
		var rot = state.transform.get_rotation()
#		print("stabilizing " + str(rot))
		
		if rot == PI*2 or rot == 0:
#			print("stabilized")
			stabilizing = false
			stabilized = true
#		elif abs(rot) < 0.01:
		elif abs(rot) > PI - 0.01:
#			print(str(abs(rot), " ", PI - 0.01))
			applied_torque = 0
			state.angular_velocity = 0
			state.transform = Transform2D(0, state.transform.get_origin())
		elif rot < 0:
			applied_torque = -30
		else:
			applied_torque = +30

	#	state.linear_velocity = state.linear_velocity.clamped(50)
	#	state.transform.origin.x += get_node('/root/Game').scroll_speed * state.step


#func _on_Player_body_entered(body: Node) -> void:
#	if "Block" in body.name:
#		on_platform = true
#		time_of_landing = OS.get_ticks_msec()


func _on_Player_body_exited(body: Node) -> void:
	if "Block" in body.name:
		on_platform = false
#		docked = false
		if time_on_ground > 1000 and velocity.length() > 0:
			boost_bonus = 50
			$Boost.play()
#			boost_ready = false
#			$Light2D.energy = 0
		time_on_ground = 0
		time_of_takeoff = OS.get_ticks_msec()
		stabilized = false
		stabilizing = false


func _on_Player_body_shape_entered(body_id: int, body: Node, body_shape: int, local_shape: int) -> void:
	if "Block" in body.name and body_shape == 1:
		on_platform = true
		time_of_landing = OS.get_ticks_msec()
		time_in_air = 0
		
