extends Area2D


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


func _on_Shard_body_entered(body: Node) -> void:
	if body.name == "Player":
		body.shard_collected()
		queue_free()
	
