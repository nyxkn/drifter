tool
extends Control

export(String) var control_name = ""
export(SettingsAudio.BUS) var bus

var _name: String = ""


func _ready() -> void:
	if control_name:
		_name = control_name
	else:
		_name = name

	$MenuSlider/Label.text = _name
	
	set_slider()
	
	$AudioStreamPlayer.bus = SettingsAudio.BUS.keys()[bus]


func set_slider():
#	$MenuSlider/HSlider.value = SettingsAudio.volume_master
	$MenuSlider/HSlider.value = SettingsAudio.get_volume(bus)
	$MenuSlider/HSlider.connect("value_changed", self, "on_value_changed")

func on_value_changed(value):
#	SettingsAudio.volume_master = value
	SettingsAudio.set_volume(bus, value)
	$AudioStreamPlayer.play()
