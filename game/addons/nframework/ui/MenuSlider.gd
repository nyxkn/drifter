tool
extends Node

export(float) var MIN_VALUE = 0
export(float) var MAX_VALUE = 100
export(float) var STEP = 1

func _ready():
	if Engine.editor_hint:
		$Label.text = name
	
	$HSlider.min_value = MIN_VALUE
	$HSlider.max_value = MAX_VALUE
	$HSlider.step = STEP

#func _enter_tree() -> void:

