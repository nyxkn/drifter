extends Node

enum LogCategory {
#    SCREEN_MANAGER, MENU, PERSISTENCE, MAP, GAME, STATE, SIGNAL, PLAYER
	FRAMEWORK, CONFIG, GAME,
}
enum LogLevel {
	DEBUG, INFO, WARN, ERROR
}

func l(category, objects, level=LogLevel.INFO):
	# Don't log if globally off
	if !Config.SHOW_LOG: return
	
	# Don't log if this LogCategory selectively hidden
	if Config.HIDE_LOG_CATEGORY.has(category) and Config.HIDE_LOG_CATEGORY[category] == true: return
	
	# Don't log if this LogLevel selectively hidden
	if Config.HIDE_LOG_LEVEL.has(level) and Config.HIDE_LOG_LEVEL[level] == true: return
	
	# Convert category index or string
	var categ_str = ""
	if category is String:
		categ_str = category.to_upper()
	if category is int and LogCategory.values().has(category):
		categ_str = str(LogCategory.keys()[category])
	
	# Add spaces to categ_str
	while categ_str.length() < 18:
		categ_str += " "
	
	# Format objects depending on type
	var objs_str = ""
	if objects is Array:
		for obj in objects:
			objs_str += " " + str(obj)
	else:
		objs_str = " " + str(objects)
	
	var result_str = "## " + categ_str + " ##  " + objs_str
	if level == LogLevel.ERROR:
		printerr(result_str)
#        push_error(resutl_str)
	elif level == LogLevel.WARN:
		print(result_str)
#        push_warning(result_str)
	else:
		print(result_str)

func d(category, objects):
	l(category, objects, LogLevel.DEBUG)
	
func i(category, objects):
	l(category, objects, LogLevel.INFO)
	
func w(category, objects):
	l(category, objects, LogLevel.WARN)

func e(category, objects):
	l(category, objects, LogLevel.ERROR)
