extends Node
## this file keeps all configuration. all of these things could be saved to a .cfg file for reloading

################################################################
# LOGGING

# Global switch for debug logs
var SHOW_LOG = true

# All LogCategories are shown by default. Add true to this Dictionary to
# prevent showing  Logs of this LogCategory
var HIDE_LOG_CATEGORY = {}

# All LogLevels are shown by default. Add true to this Dictionary to
# prevent showing Logs of this LogLevel
var HIDE_LOG_LEVEL = {}


################################################################
# RESOURCES

# you can optionally override these if you need a different scene
var INTRO_SCREEN: String = "res://addons/nframework/screens/Intro.tscn"
var END_SCREEN: String = "res://addons/nframework/screens/End.tscn"

##var MAIN_MENU: String = "res://addons/nframework/screens/MainMenu.tscn"
##var SETTINGS_MENU: String = "res://addons/nframework/screens/SettingsMenu.tscn"
#var MAIN_MENU: String = "res://addons/nframework/screens/MainMenu_LowRes.tscn"
##var SETTINGS_MENU: String = "res://addons/nframework/screens/SettingsMenu_PixelArt.tscn"
#var SETTINGS_MENU: String = "res://addons/nframework/screens/SettingsMenu_LowRes.tscn"

var MAIN_MENU: String = "res://addons/nframework/screens/MainMenu.tscn"
var SETTINGS_MENU: String = "res://addons/nframework/screens/SettingsMenu.tscn"

var THEME: String

# you should change this to your game actual entry point
var NEW_GAME: String = "res://Game.tscn"

var LEVELS_PATH: String = "res://levels/"

################################################################
# OTHER

var LOW_RES: bool = true
# enable or disable menu keyboard controls
var MENU_KEYBOARD: bool = true

#var PAUSE_ENABLED: bool = true

################################################################
# LOGIC

func _ready() -> void:
	if LOW_RES:
		THEME = "res://addons/nframework/assets/ui/theme_lowres_tex.tres"
	else:
		# ideally theme_hires would just be an override on top of lowres, without redefining everything
		# should be possible in godot 4
		THEME = "res://addons/nframework/assets/ui/theme_hires_tex.tres"

