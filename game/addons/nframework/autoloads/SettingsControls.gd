extends Node

# https://docs.godotengine.org/en/stable/classes/class_%40globalscope.html#enum-globalscope-keylist
var action_control_schemes = {
	keyboard = {
		"left": KEY_A,
		"down": KEY_S,
		"up": KEY_W,
		"right": KEY_D,
		"action1": KEY_J,
		"action2": KEY_K,
	},
	keyboard_alt = {
		"left": KEY_LEFT,
		"down": KEY_DOWN,
		"up": KEY_UP,
		"right": KEY_RIGHT,
		"action1": KEY_X, 
		"action2": KEY_Z,
	},
	joystick = {
		"left": JOY_DPAD_LEFT,
		"down": JOY_DPAD_DOWN,
		"up": JOY_DPAD_UP,
		"right": JOY_DPAD_RIGHT,
		"action1": JOY_SONY_X,
		"action2": JOY_SONY_CIRCLE,
	},
}

# actions defines the actions we actually use
var actions := ["left", "down", "up", "right", "action1", "action2", "action3"]
# control schemes defines the schemes we use
var control_schemes := [ "keyboard" ]
var action_controls := {}


func replace_action(action, event, set) -> void:
	InputMap.action_erase_event(action, action_controls[action][set])
	InputMap.action_add_event(action, event)
	action_controls[action][set] = event


func init_controls() -> void:
	# ideally load from save/config file
	# InputMap is global so it persists everywhere
	for action in actions:
		InputMap.add_action(action)
		action_controls[action] = []
		action_controls[action].resize(2)
		for i in control_schemes.size():
			action_controls[action][i] = create_action_from_set(action, control_schemes[i])
				

#	print_default_action_keys()
#	print_action_controls()
	
	
func create_action_from_set(action, scheme) -> InputEvent:
	var set = action_control_schemes[scheme]
	
	var action_control
	if action in set:
		var event: InputEvent
		if scheme.begins_with("keyboard"):
			event = InputEventKey.new()
			event.scancode = set[action]
		elif scheme.begins_with("joystick"):
			event = InputEventJoypadButton.new()
			event.button_index = set[action]			
			
		InputMap.action_add_event(action, event)
		action_control = event

	else:
		action_control = InputEventKey.new()
		
	return action_control


func print_default_action_keys() -> void:   
	for a in InputMap.get_actions():
		# exclude/choose ui_ default mappings
		if a.left(3) == 'ui_':
			var log_str = str(a, ": ")
			for i in InputMap.get_action_list(a):
				log_str += str(i.as_text(), " | ")
			Log.d("INPUT_DEFAULT", log_str)


func print_action_controls() -> void:
	pass
	for action in action_controls.keys():
		var log_str = str(action, ": ")
		for key in action_controls[action]:
			log_str += str(key.as_text(), " ")
		Log.d("INPUT", log_str)


# function lifted from GameTemplate
func get_InputEvent_name(event: InputEvent) -> String:
	var text: String = ""
	if event is InputEventKey:
#		text = "Keyboard: " + event.as_text()
		text = event.as_text()
	elif event is InputEventJoypadButton:
#		text = "Gamepad: "
#		if Input.is_joy_known(event.device):
#			text+= str(Input.get_joy_button_string(event.button_index))
#		else:
#			text += "Btn. " + str(event.button_index)
		text+= str(Input.get_joy_button_string(event.button_index))
	elif event is InputEventJoypadMotion:
#		text = "Gamepad: "
		var stick := ''
		if Input.is_joy_known(event.device):
			stick = str(Input.get_joy_axis_string(event.axis))
			text += stick + " "
		else:
			text += "Axis: " + str(event.axis) + " "
		
		if !stick.empty(): #known
			var value: int = round(event.axis_value)
			if stick.ends_with('X'):
				if value > 0:
					text += 'Rigt'
				else:
					text += 'Left'
			else:
				if value > 0:
					text += 'Down'
				else:
					text += 'Up'
		else:
			text += str(round(event.axis_value))
	
	return text
