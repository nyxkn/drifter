extends Node

#OS
var HTML5: bool = false

## we want to call these things in here to guarantee order. don't use _ready in the individual scripts

func _ready()->void:
	if OS.get_name() == "HTML5":
		HTML5 = true
#	SettingsLanguage.add_translations() #TO-DO need a way to add translations to project through the plugin
	SettingsVideo.init_video()
#	if !SettingsSaveLoad.load_settings():
#		SettingsControls.default_controls()
	SettingsControls.init_controls()
	SettingsAudio.init_volumes()
	#SettingsSaveLoad.save_settings() #Call this method to trigger Settings saving
