extends Node

# to use: add the debug scene to your scene
# then add things to track with add_reference and add_funcref

var refs = []
var funcrefs = []

onready var ui_lines = $Lines

func _ready():
	pass

func _process(delta):
	ui_lines.text = ''

	for r in refs:
#		_add_line(str(r))
		_add_data(r)

	for f in funcrefs:
		var data = f.call_func()
		_add_data(data)

func _add_data(data):
	if typeof(data) == TYPE_ARRAY:
		for e in data:
			_add_line(str(e))
	elif typeof(data) == TYPE_DICTIONARY:
		for k in data:
			_add_line(str(k) + ": " + str(data[k]))
	else:
		# assume basic unreferenceable type
#		_add_line(data if typeof(data) == TYPE_STRING else str(data))
		_add_line(str(data))

func _add_line(s):
	ui_lines.text += s + "\n"

# if it's a base type (int, float, string, vectors) and can't be passed as reference
# make a function that returns the data and pass that instead
# if data is passed as reference you can add it with add_reference

func add_reference(v):
	refs.append(v)

func add_funcref(f):
	funcrefs.append(f)
	pass
