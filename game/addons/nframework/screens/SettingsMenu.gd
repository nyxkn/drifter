extends Control

signal menu_closed

enum CONTROLS_DISPLAY { Auto, Single, Tabs } 
#export(CONTROLS_DISPLAY) var controls_display = CONTROLS_DISPLAY.Auto
var _controls_display

# this is a better system of storing things than looping and comparing names!
var ControlBind_nodes = {}

onready var ControlsList = find_node("ControlsList")
onready var ControlsTabButtons = find_node("ControlsTabButtons")
onready var ControlBind = preload("res://addons/nframework/ui/ControlBind.tscn")
onready var RebindPopup: Popup = find_node("RebindPopup")


func _ready():
#	if Settings.HTML5:
#		find_node("Borderless").visible = false
#		find_node("Scale").visible = false
	
	setup_gui()
	
	setup_controls_gui()
	
	find_node("Fullscreen").pressed = SettingsVideo.fullscreen
	find_node("Borderless").pressed = SettingsVideo.borderless
	find_node("VSync").pressed = SettingsVideo.vsync
	find_node("FPS").pressed = FPS.enabled

	find_node("Fullscreen").connect("pressed", self, "_on_Fullscreen_pressed")
	find_node("Borderless").connect("pressed", self, "_on_Borderless_pressed")
	find_node("VSync").connect("pressed", self, "_on_VSync_pressed")
	find_node("FPS").connect("pressed", self, "_on_FPS_pressed")
	
	find_node("Back").connect("pressed", self, "_on_Back_pressed")

	if Settings.HTML5:
		find_node("Borderless").visible = false
		# what about vsync and fullscreen?

	setup_custom_tabs()

	if Config.MENU_KEYBOARD:
		setup_focus()


func setup_gui() -> void:
	theme = load(Config.THEME)
	if !Config.LOW_RES:
		Utils.set_margins($MarginContainer, 16)
		Utils.set_margins($MarginContainer/VBoxContainer/MarginContainer, 0, 400)
		for child in find_node("TabsContainer").get_children():
			Utils.set_margins(child.get_node("MarginContainer"), 16, 32)
			pass
			

func _input(event) -> void:
	if event.is_action_pressed("ui_cancel"):
#		find_node("Back").grab_focus()
		_on_Back_pressed()


func setup_focus():
	Utils.setup_focus_grabs_on_mouse_entered($MarginContainer)

	find_node("Fullscreen").grab_focus()


func setup_controls_gui() -> void:
	#	if controls_display == CONTROLS_DISPLAY.Auto:
	if SettingsControls.control_schemes.size() > 1:
		_controls_display = CONTROLS_DISPLAY.Tabs
	else:
		_controls_display = CONTROLS_DISPLAY.Single
#	else:
#		_controls_display = controls_display
	
	match _controls_display:
		CONTROLS_DISPLAY.Tabs:
			ControlsTabButtons.visible = true
			
			for i in SettingsControls.control_schemes.size():
				var set_name: String = SettingsControls.control_schemes[i]
				var vbox = VBoxContainer.new()
				vbox.name = set_name.capitalize()
				ControlsTabButtons.add_child(vbox)
				ControlBind_nodes[i] = {}
				
		CONTROLS_DISPLAY.Single:
			ControlsList.visible = true
			ControlBind_nodes[0] = {}
		
	for action in SettingsControls.action_controls.keys():
#		if _controls_display == CONTROLS_DISPLAY.Single:
#			add_bind(action, SettingsControls.action_controls[action][0], 0)
#		else:
		for i in SettingsControls.control_schemes.size():
			add_bind(action, SettingsControls.action_controls[action][i], i)


func add_bind(action, control, set) -> void:
	var entry = ControlBind.instance()
	
	if _controls_display == CONTROLS_DISPLAY.Single:
		ControlsList.add_child(entry)
	else:
		ControlsTabButtons.get_child(set).add_child(entry)
		
	ControlBind_nodes[set][action] = entry	
	entry.get_node("Action").text = str(action.capitalize()) # optionally add colon after action
	update_bind(action, control, set)


func update_bind(action, event, set) -> void:
	var entry = ControlBind_nodes[set][action]
	entry.get_node("Main/Rebind").text = SettingsControls.get_InputEvent_name(event)
	entry.get_node("Main/Rebind").connect("pressed", self, "_on_Rebind_pressed", [action, set])


func _on_Rebind_pressed(action, set) -> void:
	RebindPopup.popup_centered()
	yield(RebindPopup, "NewControl")
	if RebindPopup.new_event == null:
		return
	var event: InputEvent = RebindPopup.new_event
	SettingsControls.replace_action(action, event, set)
	update_bind(action, event, set)
	ControlBind_nodes[set][action].get_node("Main/Rebind").grab_focus()


func _on_Back_pressed() -> void:
	if get_tree().get_root().get_node(self.name): # if we were loaded as a scene
		Framework.change_scene(Config.MAIN_MENU)
	else: # or as an overlay above the pause/game screen
#		visible = false
		emit_signal("menu_closed")
#		get_tree().get_root().get_node("Pause").take_focus()
#		queue_free()


func _on_Fullscreen_pressed() -> void:
	SettingsVideo.fullscreen = find_node("Fullscreen").pressed


func _on_Borderless_pressed() -> void:
	SettingsVideo.borderless = find_node("Borderless").pressed


func _on_VSync_pressed() -> void:
	SettingsVideo.vsync = find_node("VSync").pressed


func _on_FPS_pressed() -> void:
	FPS.enabled = find_node("FPS").pressed


#############################
### NoTabs version

var first_focus_video
var first_focus_audio
var first_focus_controls


func on_TabButton_focus_exited():
	if find_node("VideoTab").visible:
		first_focus_video.grab_focus()
	elif find_node("AudioTab").visible:
		first_focus_audio.grab_focus()
	elif find_node("ControlsTab").visible:
		first_focus_controls.grab_focus()
		

func setup_custom_tabs() -> void:
	first_focus_video = find_node("Fullscreen")
	first_focus_audio = find_node("Master").get_node("MenuSlider/HSlider")
	first_focus_controls = find_node("ControlsList").get_child(0).get_node("Main/Rebind")
	
	# this is called from _ready()
	show_tab("VideoTab")
	find_node("VideoTabButton").pressed = true
	
	var button_group: ButtonGroup = ButtonGroup.new()
	
	for tab_button in find_node("TabButtons").get_children():
		tab_button.group = button_group
		tab_button.connect("focus_exited", self, "on_TabButton_focus_exited")

	first_focus_audio.focus_neighbour_top = first_focus_audio.get_path_to(
		find_node("AudioTabButton")
		)

	first_focus_controls.focus_neighbour_top = first_focus_controls.get_path_to(
		find_node("ControlsTabButton")
		)


func hide_tabs() -> void:
	for tab in find_node("TabsContainer").get_children():
		tab.visible = false
		
		
func show_tab(tab_name: String) -> void:
	hide_tabs()
	find_node(tab_name).show()
	

func _on_VideoTabButton_pressed() -> void:
	show_tab("VideoTab")


func _on_AudioTabButton_pressed() -> void:
	show_tab("AudioTab")


func _on_ControlsTabButton_pressed() -> void:
	show_tab("ControlsTab")
