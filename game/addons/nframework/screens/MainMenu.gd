extends Control


func _ready():
#	theme = load(Config.THEME)
	
	if Settings.HTML5:
		find_node("Exit").visible = false
		
	find_node("NewGame").connect("pressed", self, "_on_NewGame_pressed")
	find_node("Settings").connect("pressed", self, "_on_Settings_pressed")
	find_node("Exit").connect("pressed", self, "_on_Exit_pressed")
	
	if Config.MENU_KEYBOARD:
		setup_focus()

func setup_focus():
#	for button in find_node("MenuOptions").get_children():
#		Utils.grab_focus_on_mouse_entered(button)

	Utils.setup_focus_grabs_on_mouse_entered($Control/MenuOptions)

	find_node("NewGame").grab_focus()


func _on_NewGame_pressed():
	Framework.change_scene(Config.NEW_GAME)

func _on_Settings_pressed():
	Framework.change_scene(Config.SETTINGS_MENU)

#func _on_Credits_pressed():
#    pass # Replace with function body.

func _on_Exit_pressed():
	get_tree().quit()


