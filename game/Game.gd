extends Node

var scroll_speed: int = 20
var gameover: bool
var resetting: bool = false

onready var stage := $Stage
onready var camera := $Stage/Camera2D
onready var player := $Stage/Player
onready var hud := $Overlay/HUD

var best_score: int = 0

var Stage = preload("res://Stage.tscn")

func _ready() -> void:
	Pause.pausing_allowed = true
#	$TextBox.queue_text("The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.")


func level_reset() -> void:
	stage.queue_free()
	stage = Stage.instance()
	
	camera = stage.get_node("Camera2D")
	player = stage.get_node("Player")
		
	add_child(stage)
	
	# declaring this twice. once in global variables already. very bad
	scroll_speed = 20
	gameover = false
	set_process(true)

	Transition.fade_in()
	yield(Transition, "fade_in_completed")
	


func _process(delta: float) -> void:
	camera.position.x += scroll_speed * delta
#	$HUD/Fuel.text = "Fuel: " + str($Camera2D/Player.fuel)
	$Overlay/HUD/Fuel.text = "Fuel: " + str("%.1f" % player.fuel)
	$Overlay/HUD/Shards.text = "Shards: " + str(player.shards)
	
	if not gameover:
		if player.position.x < camera.position.x - 10 or player.position.y > 360 + 10:
			gameover = true
			player.input_enabled = false
			player.get_node("Thrusters").stop()
			
			if player.shards > best_score:
				best_score = player.shards
			$Overlay/HUD/Gameover/Text/Score.text = "SHARDS COLLECTED: " + str(player.shards)
			$Overlay/HUD/Gameover/Text/Best.text = "YOUR BEST: " + str(best_score)
			$Overlay/HUD/Gameover.show()
	

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_home"):
		Framework.change_scene(Config.MAIN_MENU)
		
	if gameover:
		if event.is_action_pressed("ui_select"):
			Transition.fade_out()
			$Overlay/HUD/Gameover.hide()
	#		resetting = true
			set_process(false)
			yield(Transition, "fade_out_completed")
			yield(get_tree().create_timer(1.0), "timeout")
			level_reset()

#	if event.is_action_pressed("ui_select"):
#		$Stage/Player/Boost.play()


func _exit_tree() -> void:
	pass


func _on_Difficulty_timeout() -> void:
	scroll_speed += 1
	var bs = stage.get_node("BlockSpawner")
	var ss = stage.get_node("ShardSpawner")
	bs.wait_time = bs.wait_time + 0.2
	Log.d("DIFFICULTY", str(scroll_speed, " ", bs.wait_time))
